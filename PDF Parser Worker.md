# PDF Parser Worker

### Rabbit MQ Channels
* Channels

  * **Master - to - Extractor**

    * **Channel Name**: extractor
    * params

      * job_id: String
      * tarball_location: : String
    * **Observe By**: Master
      * Update Extraction Completed
  * **Extractor - to - Parser**
    * **Channel Name**: `parse`
    * params

      * job_id: String
      * file_id: String
      * file_location: String
    * **Observe By**: Master 

      * Update PDF Status
        * job_id: String
        * file_id: String
  * **Parser to Master**

    * **Channel Name**: parse_complete
    * params

      * job_id: String
      * file_id: String
    * **Observe By**: Master

      * job_id: String
      * file_id: String
  * **Master to Compressor**

    * **Channel Name**: compression
    * params

      * job_id: String
    * **Observe by: Master**
      * job_id: String

* **Redis**

  ```json
  {
      "<job_id>": {
          "extract_complete": Boolean,
          "files": {
              "<file_id>": {
                  "name": String,
                  "parse": Boolean
              }
          }
      }
  }
  ```
