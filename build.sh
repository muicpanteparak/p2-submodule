#!/bin/bash

PROD_PATH="prod/web"
UNZP_PATH="prod/webcontroller-1.0-SNAPSHOT"

cd webcontroller
sbt dist
if [ -d "$PROD_PATH" ]; then
    echo Removing ${PROD_PATH}
    rm -rf ${PROD_PATH}
    mkdir -p ${PROD_PATH}
fi

if [ -d "$UNZP_PATH" ]; then
    echo Removing ${UNZP_PATH}
    rm -rf ${UNZP_PATH}
    mkdir -p ${UNZP_PATH}
fi

unzip target/universal/webcontroller-1.0-SNAPSHOT.zip \
    -d prod
mv ${UNZP_PATH}/* ${PROD_PATH}
rm -rf ${UNZP_PATH}
cd ../
docker-compose up -d --build
